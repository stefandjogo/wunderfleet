<?php 
require 'connect.php';

$street = isset($_POST['street']) && !empty($_POST['street']) ? $_POST['street'] : '';
$house_number = isset($_POST['house_number']) && !empty($_POST['house_number']) ? $_POST['house_number'] : '';
$zip_code = isset($_POST['zip_code']) && !empty($_POST['zip_code']) ? $_POST['zip_code'] : '';
$city = isset($_POST['city']) && !empty($_POST['city']) ? $_POST['city'] : '';

$_SESSION['street'] = isset($_SESSION['street']) && ($_SESSION['street'] == $street || empty($street)) ? $_SESSION['street'] : $street;
$_SESSION['house_number'] = isset($_SESSION['house_number']) && ($_SESSION['house_number'] == $house_number || empty($house_number)) ? $_SESSION['house_number'] : $house_number;
$_SESSION['zip_code'] = isset($_SESSION['zip_code']) && ($_SESSION['zip_code'] == $zip_code || empty($zip_code)) ? $_SESSION['zip_code'] : $zip_code;
$_SESSION['city'] = isset($_SESSION['city']) && ($_SESSION['city'] == $city || empty($city)) ? $_SESSION['city'] : $city;

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Wunderfleet.</title>
    </head>
    <body>
        <?php
        // put your code here
        
        
        ?>
        <form action="finish.php" name="form" id="form" method="post" enctype="multipart/form-data">
            
            
            <div style="width: 400px; margin: 30px auto">
                <table >
                <thead>
                    <tr>
                        <th colspan="2" align="center">INSERT YOUR PERSONAL <br/>INFORMATIONS - STEP 3:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="">
                        <td colspan="2">
                            
                            <?php 
                            
                            foreach ($_SESSION as $key => $value) {
                                echo '<input style="display: block; width: 400px" type="text" readonly name="'.$key.'" id="'.$key.'" value="'.$value.'">';
                            }
                            
                            ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input required="required" style="display: block; width: 400px" type="text" name="account_owner" id="account_owner" placeholder="Your account owner" value=""></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input style="display: block; width: 400px" type="text" name="iban" id="iban" placeholder="Your iban" value=""></td>
                    </tr>
                    <tr>
                        <td align="left"><a href="step_2.php">Back to step 2</a></td>
                        <td align="right"><input type="submit" name="submit" id="submit" value="Finish"></td>
                    </tr>
                </tbody>
            </table>
            </div>
            
            
            
            
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
        
           
        
        </script>
    </body>
</html>
