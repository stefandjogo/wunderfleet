# Wunderfleet
I could have made form constructed in tabs - so the user would be on one side and could quickly go to next and prev as a standard progress form, so you can solve repeat the html code and including connect.php files each time. Also, for keeping the data safe in the session, I could keep the data in Cookies.

I consider the database too simple to have any need for improvement

Things that could be done better in all input fields should be prevented from entering words such as select, insert, update, delete, drop because of SQL injection. (I have already prepared and executed).

As far as advancements are concerned, we could have done OOP programming
but the program is too small for it to make a difference