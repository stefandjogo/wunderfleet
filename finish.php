<?php 
require 'connect.php';

$account_owner = isset($_POST['account_owner']) && !empty($_POST['account_owner']) ? $_POST['account_owner'] : '';
$iban = isset($_POST['iban']) && !empty($_POST['iban']) ? $_POST['iban'] : '';

$street = isset($_SESSION['street']) && !empty($_SESSION['street']) ? $_SESSION['street'] : '';
$house_number = isset($_SESSION['house_number']) && !empty($_SESSION['house_number']) ? $_SESSION['house_number'] : '';
$zip_code = isset($_SESSION['zip_code']) && !empty($_SESSION['zip_code']) ? $_SESSION['zip_code'] : '';
$city = isset($_SESSION['city']) && !empty($_SESSION['city']) ? $_SESSION['city'] : '';
    
$first_name = isset($_SESSION['first_name']) && !empty($_SESSION['first_name']) ? $_SESSION['first_name'] : '';
$last_name = isset($_SESSION['last_name']) && !empty($_SESSION['last_name']) ? $_SESSION['last_name'] : '';
$phone_number = isset($_SESSION['phone_number']) && !empty($_SESSION['phone_number']) ? $_SESSION['phone_number'] : '';


/*UPISATI U BAZU PRVO DA BI SE DOBIO ID*/

$insert = $pdo->prepare("insert into customers "
        . " (first_name, last_name  , phone_number  , street    , house_number  , zip_code  , city  , account_owner , iban) "
        . " values "
        . " (?         , ?          ,?              ,?          ,?              ,?          ,?      ,?              ,?     ) ");

$insert->execute(array($first_name, $last_name, $phone_number, $street, $house_number, $zip_code, $city, $account_owner, $iban));

if($insert){
    
    $customer_id = $pdo->query("select max(id) as id from customers")->fetch()['id'];
    
    /*END INSERT INTO DATABASE*/
    // The data to send to the API
    $postData = array(
        'customerId' =>  $customer_id,
        'iban' => $iban,
        'owner' => $account_owner
    );

    // Setup cURL
    $ch = curl_init('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));

    // Send the request
    $response = curl_exec($ch);

    // Check for errors
    if($response === FALSE){
        die(curl_error($ch));
    }
    // Decode the response
    $responseData = json_decode($response, TRUE);
    
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if($httpcode == 200){
        $payment_data_id = $responseData['paymentDataId'];
    
        $update = $pdo->prepare("update customers set payment_data_id = ? where id = ?");

        $update->execute(array($payment_data_id, $customer_id));

        if($update){
            echo 'SUCCESS: ' . $payment_data_id;
            session_destroy();
            echo '<br><a href="/">BACK TO INDEX PAGE</a>';
        }else{
            echo 'UPDATE DATABASE ERRROR';
            echo '<br><a href="/">BACK TO INDEX PAGE</a>';
        }
    }else{
        $payment_err = $responseData['error'];
        echo 'ERROR ' .$httpcode .' : ' . $payment_err;
        echo '<br><a href="/">BACK TO INDEX PAGE</a>';
    }
    
}else{
    echo 'INSERT DATABASE ERRROR';
    echo '<br><a href="/">BACK TO INDEX PAGE</a>';
}






?>
