<?php
session_start();

define('P_HOST', 'localhost');
define('P_USERNAME', 'root');
define('P_PASSWORD', '');
define('P_DATABASE', 'testing');


$servername_sql = P_HOST;
$username_sql = P_USERNAME;
$database_sql = P_DATABASE;
$password_sql = P_PASSWORD;

$sql = "mysql:dbname=$database_sql;host=$servername_sql";

$dsn_Options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

// Create a new connection to the MySQL database using PDO, $my_Db_Connection is an object
try {
    $pdo = new PDO($sql, $username_sql, $password_sql, $dsn_Options);
//    echo "Connected successfully";
} catch (PDOException $error) {
//    echo 'Connection error: ' . $error->getMessage();
}

?>
