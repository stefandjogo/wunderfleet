<?php 
require 'connect.php';

$street = isset($_SESSION['street']) && !empty($_SESSION['street']) ? $_SESSION['street'] : '';
$house_number = isset($_SESSION['house_number']) && !empty($_SESSION['house_number']) ? $_SESSION['house_number'] : '';
$zip_code = isset($_SESSION['zip_code']) && !empty($_SESSION['zip_code']) ? $_SESSION['zip_code'] : '';
$city = isset($_SESSION['city']) && !empty($_SESSION['city']) ? $_SESSION['city'] : '';
    
$first_name = isset($_SESSION['first_name']) && !empty($_SESSION['first_name']) ? $_SESSION['first_name'] : '';
$last_name = isset($_SESSION['last_name']) && !empty($_SESSION['last_name']) ? $_SESSION['last_name'] : '';
$phone_number = isset($_SESSION['phone_number']) && !empty($_SESSION['phone_number']) ? $_SESSION['phone_number'] : '';



if(empty($first_name) || empty($last_name) || empty($phone_number)){
    header('Location: step_1.php');
}else if(empty($street) || empty($house_number) || empty($zip_code) || empty($city)){
    header('Location: step_2.php');
}else{
    header('Location: step_3.php');
}





?>
