<?php 
require 'connect.php';


$first_name = isset($_POST['first_name']) && !empty($_POST['first_name']) ? $_POST['first_name'] : '';
$last_name = isset($_POST['last_name']) && !empty($_POST['last_name']) ? $_POST['last_name'] : '';
$phone_number = isset($_POST['phone_number']) && !empty($_POST['phone_number']) ? $_POST['phone_number'] : '';

$_SESSION['first_name'] = isset($_SESSION['first_name']) && ($_SESSION['first_name'] == $first_name || empty($first_name)) ? $_SESSION['first_name'] : $first_name;
$_SESSION['last_name'] = isset($_SESSION['last_name']) && ($_SESSION['last_name'] == $last_name || empty($last_name)) ? $_SESSION['last_name'] : $last_name;
$_SESSION['phone_number'] = isset($_SESSION['phone_number']) && ($_SESSION['phone_number'] == $phone_number || empty($phone_number)) ? $_SESSION['phone_number'] : $phone_number;

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Wunderfleet.</title>
    </head>
    <body>
        <?php
        // put your code here
        
        
        ?>
        <form action="step_3.php" method="post" enctype="multipart/form-data">
            
            
            <div style="width: 400px; margin: 30px auto">
                <table >
                <thead>
                    <tr>
                        <th colspan="2" align="center">INSERT YOUR PERSONAL <br/>INFORMATIONS - STEP 2:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2"><input required="required" style="display: block; width: 400px" type="text" name="street" id="street" placeholder="Your street name" value="<?php echo $_SESSION['street'] ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input required="required" style="display: block; width: 400px" type="text" name="house_number" id="house_number" placeholder="Your house number" value="<?php echo $_SESSION['house_number'] ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input required="required" style="display: block; width: 400px" type="text" name="zip_code" id="zip_code" placeholder="Your zip code" value="<?php echo $_SESSION['zip_code'] ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input required="required" style="display: block; width: 400px" type="text" name="city" id="city" placeholder="Your city" value="<?php echo $_SESSION['city'] ?>"></td>
                    </tr>
                    <tr>
                        <td align="left"><a href="step_1.php">Back to step 1</a></td>
                        <td align="right"><input type="submit" name="submit" id="submit" value="Next step"></td>
                    </tr>
                </tbody>
            </table>
            </div>
            
            
            
            
        </form>
    </body>
</html>
